<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/bootstrap-4.0.0.css',
        'css/site.css'
    ];

    public $js = [
        'js/bootstrap-4.0.0.js',
        'js/custom.js',
        'js/jquery-3.2.1.min.js',
        'js/popper.min.js',
        'js/jquery.search.js',
        'js/jquery.favourites.js'
    ];

    public $depends = [

    ];
}
