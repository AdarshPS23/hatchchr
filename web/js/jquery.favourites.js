var FavouritesService = function($)
{
    function initializeUpdateFavourites()
    {
        var url =  "<?php echo BaseUrl::base(true) ;?>"; 
        $(".favourites").click(function(){
            return $.ajax({
                type: "PUT",
                url: url,
                data:{
                    orgId,
                    userId
                }
            }).done(function (data) {
                return data;
            });
        });
    }

    function init()
    {
        initializeUpdateFavourites();
    }

    return {
        init : init
    }

}(jQuery);

FavouritesService.init();