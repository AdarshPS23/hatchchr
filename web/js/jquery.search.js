var HatchSearchService = (function ($) {

    function initializeSearch() {

        $('#search').submit(function (e) {
            e.preventDefault();
            var eleSearchKeyword = $('#searchkeyword');

            if(eleSearchKeyword.val().trim().length > 0)
            {
                this.submit();
            }
            else{
                eleSearchKeyword.val('');
            }
        });
    }

    function init() {
       initializeSearch();
    }

    return {
        init: init
    };

}(jQuery));

HatchSearchService.init();