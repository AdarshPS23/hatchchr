<?php 
use yii\helpers\BaseUrl;
?>
<header>
    <nav class="navbar navbar-expand-lg navbar-dark cust-navbar">
        <a class="navbar-brand" href="<?php echo BaseUrl::base(true); ?>/home"><img src="<?php echo BaseUrl::base(true); ?>/images/logo.png"></a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse float-right" id="navbarSupportedContent">

            <div class="ml-auto">
                <div class="cust-right-menu text-right">
                    <ul>
                        <li class="emily-markey"><a href="#">Emily Markey</a></li>
                        <li class="log-out"><a href="#">Log Out</a></li>
                        <li class="home-reports"><a class="cust-blue-buton" href="<?php echo BaseUrl::base(true); ?>/home">Go to Reports Home</a></li>
                    </ul>
                </div><!-- cust-right-menu-->
            </div><!-- right nav -->

        </div> <!-- collapse -->
    </nav><!-- nav -->
</header>