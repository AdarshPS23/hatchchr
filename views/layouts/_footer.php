<footer>
	<div class="container-fluid">
		<div class="row">
			<div class="inner-footer">
				<p class="footer-copyright float-left">Copyright  © 2018 Hatch Inc. All rights reserved.</p>
				<p class="footer-terms-use float-right">Terms of Use | Privacy Policy</p>
			</div>
		</div>
	</div>	  
</footer>