<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\BaseUrl;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap -->
        <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
	    <link href="css/site.css" rel="stylesheet">
        <title><?= Html::encode($this->title) ?></title>
        <?= Html::csrfMetaTags() ?>

    </head>
<body>
<?=$this->render('_header.php')?>
<?= $content ?>
<?=$this->render('_footer.php') ?>


<script src="js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/popper.min.js"></script>
<script src="js/bootstrap-4.0.0.js"></script>
<script src="js/jquery.search.js"></script>
<script src="js/jquery.favourites.js"></script>

</body>
</html>
