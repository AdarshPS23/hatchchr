<?php 
use yii\helpers\BaseUrl;
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;

AppAsset::register($this);

$this->title = $title;
?>

<section class="favorites-heading">
	<div class="container">
		<div class="arrow">
			<div class="favorites-inner-head text-center">
					<h1><img src="<?php echo BaseUrl::base(true); ?>/images/favorite-yellow-star.png"> Favorites (2)</h1>
			</div>
		</div>
	</div>
</section>
<section class="favorites-listing">
	<div class="container">
		<div class="favorites-listing-inner mb-1">
			<div class="fav-main-list head org-name org-name-w">
				<h3>Org Name <img src="<?php echo BaseUrl::base(true); ?>/images/down-gray-arrow.png"></h3>
			</div>
			<div class="fav-main-list head issues issues-w">
				<h3>Issues <img src="<?php echo BaseUrl::base(true); ?>/images/down-gray-arrow.png"></h3>
			</div>
			<div class="fav-main-list head opportunities opportunities-w">
				<h3>Opportunities <img src="<?php echo BaseUrl::base(true); ?>/images/down-gray-arrow.png"></h3>
			</div>
			<div class="fav-main-list head overall-health overall-health-w">
				<h3>Overall Health <img src="<?php echo BaseUrl::base(true); ?>/images/down-gray-arrow.png"></h3>
			</div>
			<div class="fav-main-list head activation activation-w">
				<h3>Activation</h3>
			</div>
			<div class="fav-main-list head adoption adoption-w">
				<h3>Adoption</h3>
			</div>
			<div class="fav-main-list head retention retention-w">
				<h3>Retention</h3>
			</div>
		</div>
		<div class="favorites-listing-inner all-side-border text-center">
			<div class="fav-main-list list org-name org-name-w align-self-center">
				<h2 class="text-left pl-2"><img src="<?php echo BaseUrl::base(true); ?>/images/yellow-star.png"> &nbsp;&nbsp;Sunnyside Centers</h2>
			</div>
			<div class="fav-main-list list issues issues-w align-self-center red-col">
				<h4>1</h4>
			</div>
			<div class="fav-main-list list opportunities opportunities-w align-self-center green-col">
				<h4>3</h4>
			</div>
			<div class="fav-main-list list overall-health overall-health-w align-self-center">
				<span class="health-rating bg-red-col white-col">3.4</span>
			</div>
			<div class="fav-main-list list activation activation-w align-self-center">
				<!-- staps -->
				<div class="wizard">
					<div class="wizard_steps">
						<nav class="steps">
							<div class="step">
								<div class="step_content">
									<p class="step_number border-blue"><span class="bg-green-col white-col">1</span></p>
									<small>Delivery</small>
									<div class="lines">
										<div class="line -background bg-col-blue"></div>
										<div class="line -progress"></div>
									</div>
								</div>
							</div>
							<div class="step">
								<div class="step_content">
									<p class="step_number border-blue"><span class="bg-red-col white-col">1.5</span></p>
									<small>Installation</small>
									<div class="lines">
										<div class="line -background bg-col-blue"></div>
									</div>
								</div>
							</div>
							<div class="step">
								<div class="step_content">
									<p class="step_number border-blue"><span>0</span></p>
									<small>Training</small>
									<div class="lines">
										<div class="line -background"></div>
									</div>
								</div>
							</div>
						</nav>
					</div>
				</div>
				<!-- staps End -->
			</div>
			<div class="fav-main-list list adoption adoption-w align-self-center">
				<!-- staps -->
				<div class="wizard">
					<div class="wizard_steps">
						<nav class="steps">
							<div class="step">
								<div class="step_content">
									<p class="step_number"><span>0</span></p>
									<small>Onboarding</small>
									<div class="lines">
										<div class="line -background"></div>
										<div class="line -progress"></div>
									</div>
								</div>
							</div>
							<div class="step">
								<div class="step_content">
									<p class="step_number"><span>0</span></p>
									<small>Initial Usage</small>
									<div class="lines">
										<div class="line -background"></div>
										<div class="line -progress"></div>
									</div>
								</div>
							</div>
						</nav>
					</div>
				</div>
				<!-- staps End -->
			</div>
		<div class="fav-main-list list retention retention-w align-self-center">
			<!-- staps -->
			<div class="wizard">
				<div class="wizard_steps">
					<nav class="steps">
						<div class="step">
							<div class="step_content">
								<p class="step_number"><span>0</span></p>
								<small>Ongoing Usage</small>
								<div class="lines">
									<div class="line -background"></div>
									<div class="line -progress"></div>
								</div>
							</div>
						</div>
						<div class="step">
							<div class="step_content">
								<p class="step_number"><span>0</span></p>
								<small>Maintenance</small>
							<!--<div class="lines">
									<div class="line -background">
									</div>
								</div>-->
							</div>
						</div>
					</nav>
				</div>
			</div>
			<!-- staps End -->
		</div>
	</div>
	<!-- Favorites Listing inner -->
	<div class="favorites-listing-inner all-side-border text-center border-top-0">
		<div class="fav-main-list list org-name org-name-w align-self-center">
			<h2 class="text-left pl-2"><img src="<?php echo BaseUrl::base(true) ?>/images/yellow-star.png"> &nbsp;&nbsp;Piedmont School District</h2>
		</div>
		<div class="fav-main-list list issues issues-w align-self-center red-col">
			<h4>1</h4>
		</div>
		<div class="fav-main-list list opportunities opportunities-w align-self-center green-col">
			<h4>5</h4>
		</div>
		<div class="fav-main-list list overall-health overall-health-w align-self-center">
			<span class="health-rating bg-green-col white-col">4.2</span>
		</div>
		<div class="fav-main-list list activation activation-w align-self-center">
			<!-- staps -->
			<div class="wizard">
				<div class="wizard_steps">
					<nav class="steps">
						<div class="step">
							<div class="step_content">
								<p class="step_number border-blue"><span class="bg-green-col white-col">1</span></p>
								<small>Delivery</small>
								<div class="lines">
									<div class="line -background bg-col-blue"></div>
									<div class="line -progress"></div>
								</div>
							</div>
						</div>
						<div class="step">
							<div class="step_content">
								<p class="step_number border-blue"><span class="bg-green-col white-col">5</span></p>
								<small>Installation</small>
								<div class="lines">
									<div class="line -background bg-col-blue"></div>
								</div>
							</div>
						</div>
						<div class="step">
							<div class="step_content">
								<p class="step_number border-blue"><span class="bg-orange-col white-col">3.6</span></p>
								<small>Training</small>
								<div class="lines">
									<div class="line -background bg-col-blue"></div>
								</div>
							</div>
						</div>
					</nav>
				</div>
			</div>
			<!-- staps End -->
		</div>
		<div class="fav-main-list list adoption adoption-w align-self-center">
			<!-- staps -->
			<div class="wizard">
				<div class="wizard_steps">
					<nav class="steps">
						<div class="step">
							<div class="step_content">
								<p class="step_number border-blue"><span class="bg-green-col white-col">5</span></p>
								<small>Onboarding</small>
								<div class="lines">
									<div class="line -background bg-col-blue"></div>
								</div>
							</div>
						</div>
						<div class="step">
							<div class="step_content">
								<p class="step_number border-blue"><span class="bg-green-col white-col">4.3</span></p>
								<small>Initial Usage</small>
								<div class="lines">
									<div class="line -background bg-col-blue"></div>
								</div>
							</div>
						</div>
					</nav>
				</div>
			</div>
			<!-- staps End -->
		</div>
		<div class="fav-main-list list retention retention-w align-self-center">
				<!-- staps -->
				<div class="wizard">
					<div class="wizard_steps">
						<nav class="steps">
							<div class="step">
								<div class="step_content">
									<p class="step_number border-blue"><span class="bg-red-col white-col">2.8</span></p>
									<small>Ongoing Usage</small>
									<div class="lines">
										<div class="line -background bg-col-blue"></div>
									</div>
								</div>
							</div>
							<div class="step">
								<div class="step_content">
									<p class="step_number border-blue"><span class="bg-red-col white-col">3.2</span></p>
									<small>Maintenance</small>
								<!--<div class="lines">
										<div class="line -background">
										</div>
									</div>-->
								</div>
							</div>
						</nav>
					</div>
				</div>
					<!-- staps End -->
			</div>
		</div>
		<!-- Favorites Listing inner -->
	</div>
</section>
<section class="main-search">
	<div class="container">
		<div class="arrow">
			<div class="organizations-but text-center">
				<a class="cust-blue-buton" href="#">View All Organizations</a>
			</div>
		</div>
	</div>
</section>