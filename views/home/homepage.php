<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\BaseUrl;

$this->title = $title;
?>
<section class="main-emily">
	<div class="container">
		<div class="arrow">
			<div class="innner-emily text-center">
				<h1>Hi Emily!</h1>
				<h3>You haven’t added any <span>Favorite Organizations</span> yet.</h3>
				<ul class="star-points">
					<li><img src="<?php echo BaseUrl::base(true); ?>/images/gray-bul-icon.jpg"> &nbsp;To get started, search for an org below and then press the <img src="<?php echo BaseUrl::base(true); ?>/images/gray-star.png"> to set it as a <span>Favorite</span> <img src="<?php echo BaseUrl::base(true); ?>/images/yellow-star.png"> .</li>  
					<li><img src="<?php echo BaseUrl::base(true); ?>/images/gray-bul-icon.jpg"> &nbsp;Favorites will show up here with an overview of their health, issues, and opportunities.</li>
				</ul>
			</div>
		</div>
	</div>
</section>	 
<section class="main-search">
	<div class="container">
		<div class="arrow">
			<?=$this->render('_search.php') ?>	
			<div class="organizations-but text-center">
				<a class="cust-blue-buton" href="#">View All Organizations</a>
			</div>
		</div>
	</div>	  
</section>