<?php 
use yii\helpers\BaseUrl; 
?>

<div class="search-inner">
	<form class="form-inline my-lg-0 justify-content-center search-for-org" id="search" action="<?php echo BaseUrl::base(true); ?>/search">
		<input class="form-control mr-sm-2" id="searchkeyword" name="q" type="search"  maxlength="75" placeholder="Search for an Org, Site, or Contact Name" aria-label="Search">
		<button class="btn btn-outline-success my-2 my-sm-0 submit" type="submit">Search</button>
	</form>
</div>