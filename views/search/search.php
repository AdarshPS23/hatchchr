<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\BaseUrl;

$this->title = $title;
?>
<section class="favorites-heading">
    <div class="container">
        <div class="arrow">
            <div class="favorites-inner-head text-center">
                <h1>Search Results (2)</h1>
            </div>
        </div>
    </div>
</section>
<section class="favorites-listing">
    <div class="container">
        <div class="favorites-listing-inner search-result-list text-center">
            <div class="search-main-list align-self-center">
                <div class="search-address-details">
                    <h2><img src="<?php echo BaseUrl::base(true); ?>/images/yellow-star.png"> &nbsp;&nbsp; Piedmont School District</h2>
                    <h3>MAS ID: <span class="mas-id">4982479693555</span></h3>
                    <h3>Type:<span class="hatch">Hatch 100</span></h3>
                </div>
                <div class="search-address-details mt-3">
                    <h3>Relevant Sites:</h3>
                    <h4>Meadowgreen East</h4>
                </div>
            </div>

            <div class="search-main-list align-self-end">
                <div class="search-address-details mt-8">
                    <h3 class="ml-0">Location: <span class="Loca"> Winston-Salem, NC</span></h3>
                    <ul>
                        <li>
                            <h3 class="ml-0">Issues:<span class="mas-id">2</span></h3></li>
                        <li>
                            <h3 class="float-right">Opportunities: <span class="mas-id">5</span></h3></li>
                    </ul>
                </div>
                <div class="search-address-details mt-5">
                    <h3 class="ml-0">Location: <span class="Loca"> Winston-Salem, NC</span></h3>
                    <ul>
                        <li>
                            <h3 class="ml-0">Issues:<span class="mas-id">2</span></h3></li>
                        <li>
                            <h3 class="float-right">Opportunities: <span class="mas-id">5</span></h3></li>
                    </ul>
                </div>
            </div>

            <div class="steps-outer">

                <div class="fav-main-list list activation activation-w align-self-center">
                    <!-- staps -->
                    <div class="wizard">
                        <h6 class="site-health-sub-head">Activation</h6>
                        <div class="wizard_steps">
                            <nav class="steps">

                                <div class="step">
                                    <div class="step_content">
                                        <p class="step_number border-blue"><span class="bg-green-col white-col"><img src="<?php echo BaseUrl::base(true); ?>/images/good.png"></span></p>
                                        <small>Delivery</small>
                                        <div class="lines">
                                            <div class="line -background bg-col-blue">
                                            </div>

                                            <div class="line -progress">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="step">
                                    <div class="step_content">
                                        <p class="step_number border-blue"><span class="bg-green-col white-col"><img src="<?php echo BaseUrl::base(true); ?>/images/good.png"></span></p>
                                        <small>Installation</small>
                                        <div class="lines">
                                            <div class="line -background bg-col-blue">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="step">
                                    <div class="step_content">
                                        <p class="step_number border-blue"><span class="bg-red-col white-col"><img src="<?php echo BaseUrl::base(true); ?>/images/bad.png"></span></p>
                                        <small>Training</small>
                                        <div class="lines">
                                            <div class="line -background bg-col-blue">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </nav>
                        </div>
                    </div>
                    <!-- staps End -->
                </div>

                <div class="fav-main-list list adoption adoption-w align-self-center">
                    <!-- staps -->
                    <div class="wizard">
                        <h6 class="site-health-sub-head">Adoption</h6>
                        <div class="wizard_steps">
                            <nav class="steps">

                                <div class="step">
                                    <div class="step_content">
                                        <p class="step_number border-blue"><span class="bg-green-col white-col"><img src="<?php echo BaseUrl::base(true); ?>/images/good.png"></span></p>
                                        <small>Onboarding</small>
                                        <div class="lines">
                                            <div class="line -background bg-col-blue">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="step">
                                    <div class="step_content">
                                        <p class="step_number border-blue"><span class="bg-green-col white-col"><img src="<?php echo BaseUrl::base(true); ?>/images/good.png"></span></p>
                                        <small>Initial Usage</small>
                                        <div class="lines">
                                            <div class="line -background bg-col-blue">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </nav>
                        </div>
                    </div>
                    <!-- staps End -->
                </div>

                <div class="fav-main-list list retention retention-w align-self-center">
                    <!-- staps -->
                    <div class="wizard">
                        <h6 class="site-health-sub-head">Retention</h6>
                        <div class="wizard_steps">
                            <nav class="steps">

                                <div class="step">
                                    <div class="step_content">
                                        <p class="step_number border-blue"><span class="bg-red-col white-col"><img src="<?php echo BaseUrl::base(true); ?>/images/bad.png"></span></p>
                                        <small>Ongoing Usage</small>
                                        <div class="lines">
                                            <div class="line -background bg-col-blue">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="step">
                                    <div class="step_content">
                                        <p class="step_number border-blue"><span class="bg-red-col white-col"><img src="<?php echo BaseUrl::base(true); ?>/images/bad.png"></span></p>
                                        <small>Maintenance</small>
                                        <!--<div class="lines">
                                <div class="line -background">
                                </div>
                            </div>-->
                                    </div>
                                </div>

                            </nav>
                        </div>
                    </div>
                    <!-- staps End -->
                </div>
            </div>
        </div>
        <!-- Favorites Listing inner -->

        <div class="favorites-listing-inner search-result-list text-center border-top">
            <div class="search-main-list align-self-center">
                <div class="search-address-details">
                    <h2><img src="<?php echo BaseUrl::base(true); ?>/images/gray-star.png"> &nbsp;&nbsp; Norfolk Centers</h2>
                    <h3>MAS ID: <span class="mas-id">4982479693555</span></h3>
                    <h3>Type:<span class="hatch">Large Account </span></h3>
                </div>
                <div class="search-address-details mt-3">
                    <h3>Relevant Sites:</h3>
                    <h4>Meadowgreen Eastern</h4>
                </div>
            </div>

            <div class="search-main-list align-self-end">
                <div class="search-address-details mt-8">
                    <h3 class="ml-0">Location: <span class="Loca"> Winston-Salem, NC</span></h3>
                    <ul>
                        <li>
                            <h3 class="ml-0">Issues:<span class="mas-id">2</span></h3></li>
                        <li>
                            <h3 class="float-right">Opportunities: <span class="mas-id">5</span></h3></li>
                    </ul>
                </div>
                <div class="search-address-details mt-5">
                    <h3 class="ml-0">Location: <span class="Loca"> Winston-Salem, NC</span></h3>
                    <ul>
                        <li>
                            <h3 class="ml-0">Issues:<span class="mas-id">2</span></h3></li>
                        <li>
                            <h3 class="float-right">Opportunities: <span class="mas-id">5</span></h3></li>
                    </ul>
                </div>
            </div>

            <div class="steps-outer">

                <div class="fav-main-list list activation activation-w align-self-center">
                    <!-- staps -->
                    <div class="wizard">
                        <h6 class="site-health-sub-head">Activation</h6>
                        <div class="wizard_steps">
                            <nav class="steps">

                                <div class="step">
                                    <div class="step_content">
                                        <p class="step_number border-blue"><span class="bg-green-col white-col"><img src="<?php echo BaseUrl::base(true); ?>/images/good.png"></span></p>
                                        <small>Delivery</small>
                                        <div class="lines">
                                            <div class="line -background bg-col-blue">
                                            </div>

                                            <div class="line -progress">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="step">
                                    <div class="step_content">
                                        <p class="step_number border-blue"><span class="bg-green-col white-col"><img src="<?php echo BaseUrl::base(true); ?>/images/good.png"></span></p>
                                        <small>Installation</small>
                                        <div class="lines">
                                            <div class="line -background bg-col-blue">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="step">
                                    <div class="step_content">
                                        <p class="step_number border-blue"><span class="bg-red-col white-col"><img src="<?php echo BaseUrl::base(true); ?>/images/good.png"></span></p>
                                        <small>Training</small>
                                        <div class="lines">
                                            <div class="line -background bg-col-blue">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </nav>
                        </div>
                    </div>
                    <!-- staps End -->
                </div>

                <div class="fav-main-list list adoption adoption-w align-self-center">
                    <!-- staps -->
                    <div class="wizard">
                        <h6 class="site-health-sub-head">Adoption</h6>
                        <div class="wizard_steps">
                            <nav class="steps">

                                <div class="step">
                                    <div class="step_content">
                                        <p class="step_number border-blue"><span class="bg-green-col white-col"><img src="<?php echo BaseUrl::base(true); ?>/images/bad.png"></span></p>
                                        <small>Onboarding</small>
                                        <div class="lines">
                                            <div class="line -background bg-col-blue">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="step">
                                    <div class="step_content">
                                        <p class="step_number border-blue"><span class="bg-green-col white-col"><img src="<?php echo BaseUrl::base(true); ?>/images/bad.png"></span></p>
                                        <small>Initial Usage</small>
                                        <div class="lines">
                                            <div class="line -background bg-col-blue">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </nav>
                        </div>
                    </div>
                    <!-- staps End -->
                </div>

                <div class="fav-main-list list retention retention-w align-self-center">
                    <!-- staps -->
                    <div class="wizard">
                        <h6 class="site-health-sub-head">Retention</h6>
                        <div class="wizard_steps">
                            <nav class="steps">

                                <div class="step">
                                    <div class="step_content">
                                        <p class="step_number border-blue"><span class="bg-red-col white-col"><img src="<?php echo BaseUrl::base(true); ?>/images/bad.png"></span></p>
                                        <small>Ongoing Usage</small>
                                        <div class="lines">
                                            <div class="line -background bg-col-blue">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="step">
                                    <div class="step_content">
                                        <p class="step_number border-blue"><span class="bg-red-col white-col"><img src="<?php echo BaseUrl::base(true); ?>/images/bad.png"></span></p>
                                        <small>Maintenance</small>
                                        <!--<div class="lines">
                                <div class="line -background">
                                </div>
                            </div>-->
                                    </div>
                                </div>

                            </nav>
                        </div>
                    </div>
                    <!-- staps End -->
                </div>

            </div>

        </div>
        <!-- Favorites Listing inner -->
    </div>
</section>