<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;


class HomeController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
        $favorites = [];
        if(count($favorites) !== 0)
        {
            $pageTitle = 'favourites';
            return $this->render('favourites', array('title' =>$pageTitle));
        }
        else
        {
            $pageTitle = 'home';
            return $this->render('homepage', array('title' => $pageTitle));
        }
    }
}