<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;


class HomeController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays Organizations.
     *
     * @return string
     */
    public function actionIndex()
    {
            $pageTitle = 'Organizations';
            return $this->render('Organizations', array('title' => $pageTitle));
    }

    /**
     * Add or Remove favourite organization.
     *
     * @return string
     */
    public function actionUpdateFavourites($userId, $orgId)
    {

    }
}